<?php

/**
 * @param array
 *
 * @return string
 */
function solution($a)
{
    $n = count($a);
    $s[-1] = 0;

    for ($i = 0; $i < $n; $i ++) {
        $a[$i] = $a[$i] & 1;
        $s[$i] = $s[$i-1] + $a[$i];
    }

    if (!($s[$n-1] & 1)) {
        return sprintf('%d,%d', 0, $n - 1);
    }

    $no_solution = $n & 1;
    $m = ($n - 1) >> 1;

    if ($no_solution) {
        for ($i = 0; $i < $m; $i ++) {
            $no_solution &= !$a[$i];
        }
    }

    if ($no_solution) {
        $no_solution &= $a[$m];
    }

    if ($no_solution) {
        for ($i = $m+1; $i < $n; $i ++) {
            $no_solution &= !$a[$i];
        }
    }

    if ($no_solution) {
        return 'NO SOLUTION';
    }

    $l = null;
    $r = null;

    for ($m = ($n - 2) >> 1; $m >= 0; $m --) {
        $k = $n - 2*$m - 1;

        if ($a[$n-1-$m] && ($s[$n-1-$m-1] - $k <= 0) && ($s[$n-1] - $s[$n-1-$m] == 0)) {
            for ($i = 0; $i+$k <= $n-1-$m; $i ++) {
                if (!(($s[$i+$k-1] - $s[$i-1]) & 1) && ($s[$n-1-$m-1] - $s[$i+$k-1] + $s[$i-1] == 0)) {
                    if (!isset($l) || $i < $l || ($i == $l && $k-1 < $r - $l)) {
                        $l = $i;
                        $r = $i + $k - 1;
                    }
                }
            }
        }

        if ($a[$m] && $s[$m-1] == 0 && ($s[$n-1] - $s[$m] - $k <= 0)) {
            for ($i = $m+1; $i+$k <= $n; $i ++) {
                if (!(($s[$i+$k-1] - $s[$i-1]) & 1) && ($s[$n-1] - $s[$m] - $s[$i+$k-1] + $s[$i-1] == 0)) {
                    if (!isset($l) || $i < $l || ($i == $l && $k-1 < $r - $l)) {
                        $l = $i;
                        $r = $i + $k - 1;
                    }
                }
            }
        }
    }

    if (isset($l)) {
        return sprintf('%d,%d', $l, $r);
    }

    return 'NO SOLUTION';
}

if (__FILE__ != realpath($argv[0])) {
    return;
}

$n = intval(fgets(STDIN));

if ($n < 1) {
    throw new Exception('must be n > 0');
}

$a = array_map('intval', explode(' ', fgets(STDIN)));

if ($n != count($a)) {
    throw new Exception("must be $n numbers");
}

print(solution($a));

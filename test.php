<?php

require 'Main.php';

/**
 * @param array
 *
 * @return void
 */
function test($a)
{
    print('a: ' . implode(' ', $a) . PHP_EOL);
    $result = solution($a);

    print($result . PHP_EOL);

    if ($result != 'NO SOLUTION') {
        $b = $a;
        $t = array_map('intval', explode(',', $result));

        list($l, $r) = $t;

        for ($j = $l; $j <= $r; $j ++) {
            unset($b[$j]);
        }

        $b = array_values($b);
        print('b: ' . implode(' ', $b) . PHP_EOL);

        $result = solution($b);
        print($result . PHP_EOL);

        if ($result != 'NO SOLUTION') {
            print('wrong answer');
            exit();
        }
    }

    print('---' . PHP_EOL);
}

$a = [1, 1, 1];

for ($i = 0; $i < 10; $i ++) {
    $a[] = 2;
}

while (true) {
    shuffle($a);
    test($a);
}
